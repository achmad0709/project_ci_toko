-- phpMyAdmin SQL Dump
-- version 4.5.1
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: May 09, 2019 at 05:42 PM
-- Server version: 10.1.9-MariaDB
-- PHP Version: 7.0.1

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `toko_jaya_abadi`
--

-- --------------------------------------------------------

--
-- Table structure for table `barang`
--

CREATE TABLE `barang` (
  `kode_barang` varchar(5) NOT NULL,
  `nama_barang` varchar(150) NOT NULL,
  `harga_barang` float NOT NULL,
  `kode_jenis` varchar(5) NOT NULL,
  `flag` int(11) NOT NULL,
  `stok` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `barang`
--

INSERT INTO `barang` (`kode_barang`, `nama_barang`, `harga_barang`, `kode_jenis`, `flag`, `stok`) VALUES
('BR022', 'Celana Leging', 12000, 'JB001', 1, 0),
('BR023', 'G-string', 1400000000, 'JB001', 1, 0),
('BR024', 'CELANA BAJU', 12000, 'JB024', 1, 0),
('BR025', 'CELANA AYAM', 10000, 'JB025', 1, -8),
('BR027', 'JAKET', 200000, 'JB027', 1, 1),
('BR028', 'PULPEN', 60000, 'JB003', 1, 0),
('BR029', 'pulpen', 1200000, 'JB001', 1, -2),
('BR030', 'pulpen', 11111, 'JB001', 1, -1),
('BR031', 'Popog bayi', 12000, 'JB031', 1, 20);

-- --------------------------------------------------------

--
-- Table structure for table `jabatan`
--

CREATE TABLE `jabatan` (
  `kode_jabatan` varchar(5) NOT NULL,
  `nama_jabatan` varchar(15) NOT NULL,
  `keterangan` int(11) NOT NULL,
  `flag` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `jabatan`
--

INSERT INTO `jabatan` (`kode_jabatan`, `nama_jabatan`, `keterangan`, `flag`) VALUES
('JB001', 'operasional', 0, 1),
('JB002', 'operasional', 1, 1),
('JB003', 'manager', 1, 1),
('JB021', 'manager', 1, 1),
('JB022', 'Bos', 1, 1),
('jb555', 'manager', 0, 1);

-- --------------------------------------------------------

--
-- Table structure for table `jenis_barang`
--

CREATE TABLE `jenis_barang` (
  `kode_jenis` varchar(5) NOT NULL,
  `nama_jenis` varchar(100) NOT NULL,
  `flag` int(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `jenis_barang`
--

INSERT INTO `jenis_barang` (`kode_jenis`, `nama_jenis`, `flag`) VALUES
('JB001', 'Celana', 1),
('JB002', 'BAJU', 1),
('JB003', 'BAJU', 1),
('JB004', 'SEMPAK', 1),
('JB005', 'BAJU', 1),
('JB010', 'BAJU', 1),
('JB011', 'Jaket kulit', 1);

-- --------------------------------------------------------

--
-- Table structure for table `karyawan`
--

CREATE TABLE `karyawan` (
  `nik` varchar(100) NOT NULL,
  `nama_lengkap` varchar(150) NOT NULL,
  `tempat_lahir` varchar(100) NOT NULL,
  `tgl` date NOT NULL,
  `jenis_kelamin` varchar(1) NOT NULL,
  `alamat` text NOT NULL,
  `telp` varchar(15) NOT NULL,
  `kode_jabatan` varchar(5) NOT NULL,
  `photo` varchar(111) NOT NULL,
  `flag` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `karyawan`
--

INSERT INTO `karyawan` (`nik`, `nama_lengkap`, `tempat_lahir`, `tgl`, `jenis_kelamin`, `alamat`, `telp`, `kode_jabatan`, `photo`, `flag`) VALUES
('1704421300', 'sds', 'Jakarta', '0000-00-00', 'L', 'ccc', '05487987', 'JB001', '190402_1704421300.jpg', 1),
('1704421899', 'pono', 'Jakarta', '0000-00-00', 'L', 'jln duri', '0854656', 'JB001', 'default.png', 1),
('178712727', 'mamad', 'jakarta', '2019-04-02', 'L', 'jakarta jalan', '08541122330', 'JB001', '', 1),
('195499898', 'osiais', 'JAWA', '0000-00-00', 'L', 'jl malaka', '087877878', 'JB001', 'default.png', 1),
('7896512', 'Putri Arini sh', 'Bekasi', '0000-00-00', 'L', 'assa', '0928288282', 'JB001', 'default.png', 1);

-- --------------------------------------------------------

--
-- Table structure for table `login`
--

CREATE TABLE `login` (
  `username` varchar(100) NOT NULL,
  `password` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `login`
--

INSERT INTO `login` (`username`, `password`) VALUES
('admin', 'admin123');

-- --------------------------------------------------------

--
-- Table structure for table `pembelian_detail`
--

CREATE TABLE `pembelian_detail` (
  `id_pembelian_d` int(11) NOT NULL,
  `id_pembelian_h` int(11) NOT NULL,
  `kode_barang` varchar(5) NOT NULL,
  `qty` int(11) NOT NULL,
  `harga` float NOT NULL,
  `jumlah` float NOT NULL,
  `flag` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `pembelian_detail`
--

INSERT INTO `pembelian_detail` (`id_pembelian_d`, `id_pembelian_h`, `kode_barang`, `qty`, `harga`, `jumlah`, `flag`) VALUES
(0, 1, 'BR021', 2, 20000000, 40000000, 1),
(0, 0, 'BR021', 3, 100000000, 300000000, 1),
(3, 1, 'BR021', 2, 15000000, 2, 1),
(10, 9, 'BR027', 1, 100000, 2, 1),
(0, 0, 'BR021', 1, 14000, 14001, 1),
(0, 0, 'BR021', 10, 12000, 12010, 1);

-- --------------------------------------------------------

--
-- Table structure for table `pembelian_header`
--

CREATE TABLE `pembelian_header` (
  `id_pembelian_h` int(11) NOT NULL,
  `no_transaksi` varchar(10) NOT NULL,
  `tanggal` date NOT NULL,
  `kode_supplier` varchar(5) NOT NULL,
  `approved` int(11) NOT NULL,
  `flag` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `pembelian_header`
--

INSERT INTO `pembelian_header` (`id_pembelian_h`, `no_transaksi`, `tanggal`, `kode_supplier`, `approved`, `flag`) VALUES
(1, 'trp0988', '2019-02-14', 'SP001', 1, 1),
(0, 'TR004', '2019-04-09', 'SP001', 1, 1),
(0, 'TR005', '2019-04-10', 'SP001', 1, 1),
(1, 'TR009', '2019-01-05', 'SP003', 1, 1),
(0, 'TR90408A00', '2019-04-29', 'SP001', 1, 1),
(0, 'TR90408A00', '2019-04-29', 'SP007', 1, 1),
(0, 'TR90508A00', '2019-05-02', 'SP001', 1, 1);

-- --------------------------------------------------------

--
-- Table structure for table `penjualan_detail`
--

CREATE TABLE `penjualan_detail` (
  `id_jual_d` int(11) NOT NULL,
  `id_jual_h` int(11) NOT NULL,
  `kode_barang` varchar(5) NOT NULL,
  `qty` int(11) NOT NULL,
  `harga` float NOT NULL,
  `jumlah` float NOT NULL,
  `flag` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `penjualan_detail`
--

INSERT INTO `penjualan_detail` (`id_jual_d`, `id_jual_h`, `kode_barang`, `qty`, `harga`, `jumlah`, `flag`) VALUES
(1, 2, 'BR022', 1, 12000, 2, 1),
(2, 0, 'BR023', 1, 300000000, 3, 1),
(0, 0, 'BR021', 5, 12000, 60000, 1),
(0, 0, 'BR023', 2, 1400000000, 2800000000, 1),
(0, 2, 'BR021', 7, 12000, 84000, 1),
(0, 0, 'BR024', 2, 12000, 24000, 1),
(0, 0, 'BR024', 2, 12000, 24000, 1),
(0, 0, 'BR024', 2, 12000, 24000, 1),
(0, 0, 'BR024', 2, 12000, 24000, 1),
(0, 0, 'BR024', 2, 12000, 24000, 1),
(0, 0, 'BR024', 2, 12000, 24000, 1),
(0, 0, 'BR024', 2, 12000, 24000, 1),
(0, 0, 'BR025', 8, 10000, 80000, 1),
(0, 0, 'BR025', 8, 10000, 80000, 1),
(10, 9, 'BR027', 2, 100000, 2, 1),
(0, 0, 'BR027', 100000, 20000, 2000000000, 1),
(0, 0, 'BR027', 2, 20000, 40000, 1),
(0, 0, 'BR027', 1, 200000, 200000, 1),
(0, 0, 'BR027', 1, 200000, 200000, 1),
(0, 0, 'BR029', 2, 1200000, 2400000, 1),
(0, 0, 'BR030', 1, 11111, 11111, 1),
(0, 0, 'BR031', 5, 12000, 60000, 1),
(0, 0, 'BR031', 5, 12000, 60000, 1);

-- --------------------------------------------------------

--
-- Table structure for table `penjualan_header`
--

CREATE TABLE `penjualan_header` (
  `id_jual_h` int(11) NOT NULL,
  `no_transaksi` varchar(10) NOT NULL,
  `tanggal` date NOT NULL,
  `pembeli` varchar(250) NOT NULL,
  `flag` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `penjualan_header`
--

INSERT INTO `penjualan_header` (`id_jual_h`, `no_transaksi`, `tanggal`, `pembeli`, `flag`) VALUES
(1, 'TR004', '2019-04-09', '', 1),
(2, 'TR005', '2019-04-16', 'ACHMAD', 1),
(0, 'TR006', '2019-04-22', 'RAMDAN', 1),
(2, 'TR005', '2019-04-16', 'ACHMAD', 1),
(2, 'TR006', '2019-04-16', 'ACHMAD', 1),
(0, 'TR010', '2019-04-22', 'ramdun', 1),
(0, 'TR90413B01', '2019-04-29', 'RAMDAN', 1),
(0, 'TR90513B90', '2019-05-02', 'Poernomo', 1);

-- --------------------------------------------------------

--
-- Table structure for table `supplier`
--

CREATE TABLE `supplier` (
  `kode_supplier` varchar(5) NOT NULL,
  `nama_supplier` varchar(100) NOT NULL,
  `alamat` text NOT NULL,
  `telp` varchar(15) NOT NULL,
  `flag` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `supplier`
--

INSERT INTO `supplier` (`kode_supplier`, `nama_supplier`, `alamat`, `telp`, `flag`) VALUES
('SP001', 'Achmads', 'Jl.waduk pluit', '08976543331', 1),
('SP001', 'Achmads', 'Jl.waduk pluit', '08976543331', 1),
('SP003', 'mamamd', 'jln swadaya', '05487987', 1),
('SP004', 'Achmad', 'ssss', '05487987', 1),
('SP006', 'AchmadQe', 'jl waduk', '08976543339', 1),
('SP007', 'ramdun', 'jl.waruga', '087625152551', 1);

-- --------------------------------------------------------

--
-- Table structure for table `user`
--

CREATE TABLE `user` (
  `id_user` int(11) NOT NULL,
  `nik` varchar(10) NOT NULL,
  `email` varchar(100) NOT NULL,
  `password` varchar(32) NOT NULL,
  `tipe` int(11) NOT NULL,
  `flag` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `user`
--

INSERT INTO `user` (`id_user`, `nik`, `email`, `password`, `tipe`, `flag`) VALUES
(1, '1902001', 'user@gmail.com', '827ccb0eea8a706c4c34a16891f84e7b', 2, 1),
(2, 'admin', 'admin@gmail.com', 'e10adc3949ba59abbe56e057f20f883e', 1, 1),
(3, '1902003', 'admin21@gmail.com', 'admin123', 2, 1);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `jabatan`
--
ALTER TABLE `jabatan`
  ADD PRIMARY KEY (`kode_jabatan`);

--
-- Indexes for table `jenis_barang`
--
ALTER TABLE `jenis_barang`
  ADD PRIMARY KEY (`kode_jenis`);

--
-- Indexes for table `karyawan`
--
ALTER TABLE `karyawan`
  ADD PRIMARY KEY (`nik`);

--
-- Indexes for table `login`
--
ALTER TABLE `login`
  ADD PRIMARY KEY (`username`);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
