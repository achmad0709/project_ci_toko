<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Penjualan_model extends CI_Model
{
    //panggil nama table
    private $_table_header = "penjualan_header";
    private $_table_detail = "penjualan_detail";

    public function tampilDataPenjualan()
    {
        $query	= $this->db->query(
            "SELECT * FROM " . $this->_table_header . " WHERE flag = 1"
        );
        return $query->result();	
    }

    public function savePenjualanHeader()
    {
        $no_transaksi = $this->input->post('no_transaksi');
    $sql = $this->db->query("SELECT no_transaksi from penjualan_header where no_transaksi='$no_transaksi'");
    $cek_no_transaksi = $sql->num_rows();
    if ($cek_no_transaksi > 0)
    {
      $this->session->set_flashdata();
      redirect('penjualan/input');
    }else{
      $no_transaksi = $this->input->post('no_transaksi');
    }
        $data['no_transaksi']   = $this->input->post('no_transaksi');
        $data['tanggal']        = date('Y-m-d');
        $data['pembeli']   = $this->input->post('pembeli');
        //$data['approved']       = 1;
        $data['flag']           = 1;

        $this->db->insert($this->_table_header, $data);
    }

    public function idTransaksiTerakhir()
    {
        $query	= $this->db->query(
            "SELECT * FROM " . $this->_table_header . " WHERE flag = 1 ORDER BY id_jual_h DESC LIMIT 0,1"
        );
        $data_id = $query->result();

        foreach ($data_id as $data) {
            $last_id = $data->id_jual_h;
        }

        return $last_id;
    }

    public function tampilDataPenjualanDetail($id)
    {
        $query	= $this->db->query(
            "SELECT A.*, B.nama_barang FROM " . $this->_table_detail . " AS A INNER JOIN barang AS B ON A.kode_barang = B.kode_barang WHERE A.flag = 1 AND A.id_jual_h = '$id'"
        );  
        return $query->result();	
    }

    public function savePenjualanDetail($id)
    {
        $kode_barang    = $this->input->post('kode_barang');
        $harga_barang  = $this->Barang_model->TampilHargaBarang($kode_barang);
        $qty    = $this->input->post('qty');
        // $data['data_barang'] = $this->input->post('harga_barang');
       // echo "<pre>";
        //print_r($_FILES['']); die;
        //echo "</pre>";
      

        //return $last_id;
        
        //$data['id_jual_h'] = $id;
        //$data['kode_barang']    = $this->input->post('kode_barang');
         //$data['nama_barang']    = $this->input->post('nama_barang');

        
        $data['id_jual_h']      =$id;
        $data['kode_barang']      =$kode_barang;
        $data['qty']            = $qty;
        $data['harga']          = $harga_barang;
        $data['jumlah']         = $qty * $harga_barang;
        $data['flag']           = 1;

        $this->db->insert($this->_table_detail, $data);
    }


    public function rules()
    
    {
        return [
        [
        'field' => 'no_transaksi',
        'label' => 'no transaksi',
        'rules' => 'required|max_length[20]',
        'errors' => [
           'required' => 'no transaksi tidak boleh kosong.',
           'max_length' => 'no transaksi tidak boleh lebih dari 5 karakter.',
           ]

           ],
           [

         'field' => 'pembeli',
         'label' => 'nama pembeli',
         'rules' => 'required',
         'errors' => [
           'required' => 'nama pembeli tidak boleh kosong.',
            
                   ]
           ]
           ];
         }

          public function rulesinput()
    
    {
        return [
        [
        'field' => 'no_transaksi',
        'label' => 'no transaksi',
        'rules' => 'required|max_length[20]|is_unique[penjualan_header.no_transaksi]',
        'errors' => [
           'required' => 'no transaksi tidak boleh sama.',
           'max_length' => 'no transaksi tidak boleh lebih dari 5 karakter.',
           'is_unique' => 'no transaksi tidak boleh sama.',
           ]

           ],
           [

         'field' => 'pembeli',
         'label' => 'nama_pembeli',
         'rules' => 'required',
         'errors' => [
           'required' => 'nama_pembeli tidak boleh kosong.',
            
                   ]
           ]
           ];
         }

         
     public function rules1()
    
    {
        return [
        [
        'field' => 'kode_barang',
        'label' => 'kode barang',
        'rules' => 'required|max_length[20]',
        'errors' => [
           'required' => 'kode barang tidak boleh kosong.',
           'max_length' => 'kode barang tidak boleh lebih dari 5 karakter.',
           ]
           ],
           [
         'field' => 'qty',
         'label' => 'qty',
         'rules' => 'required|numeric',
         'errors' => [
           'required' => 'qty tidak boleh kosong.',
           'numeric' => 'qty harus angka.',
            ]
            
           ]
           ];
           


    }

    public function tampilDataPenjualanpagination($perpage, $uri, $data_pencarian)
  {
    $this->db->select('*');
    if (!empty($data_pencarian)) {
      $this->db->like('pembeli', $data_pencarian);
    }
    $this->db->order_by('id_jual_h', 'asc');
    
    $get_data = $this->db->get($this->_table_header, $perpage, $uri);
    if ($get_data->num_rows() > 0) {
      return $get_data->result();
    } else {
      return null;
    }
    
    }
    
  
  public function tombolpagination($data_pencarian)
  {
    $this->db->like('pembeli', $data_pencarian);
    $this->db->from($this->_table_header);
    $hasil = $this->db->count_all_results();
    
    $pagination['base_url'] = base_url(). 'penjualan/listpenjualan/load/';
    $pagination['total_rows'] = $hasil;
    $pagination['per_page'] = "2";
    $pagination['uri_segment'] = 4;
    $pagination['num_links'] = 2;
    
    // custom paging configuration
    $pagination['full_tag_open'] = '<div class="pagination">';
    $pagination['full_tag_close'] = '</div>';
    
    $pagination['first_link'] = 'first page';
    $pagination['first_tag_open'] = '<span class="firstlink">';
    $pagination['first_tag_close'] = '</span>';
    
    $pagination['last_link'] = 'last page';
    $pagination['last_tag_open'] = '<span class="lastlink">';
    $pagination['last_tag_close'] = '</span>';
    
    $pagination['next_link'] = 'next page';
    $pagination['next_tag_open'] = '<span class="nextlink">';
    $pagination['next_tag_close'] = '</span>';
    
    $pagination['prev_link'] = 'prev page';
    $pagination['prev_tag_open'] = '<span class="prevlink">';
    $pagination['prev_tag_close'] = '</span>';
    
  
    $pagination['cur_tag_open'] = '<span class="curlink">';
    $pagination['cur_tag_close'] = '</span>';
    
    $pagination['num_tag_open'] = '<span class="numlink">';
    $pagination['num_tag_close'] = '</span>';
    
    $this->pagination->initialize($pagination);
    $hasil_pagination = $this->tampilDataPenjualanpagination($pagination['per_page'],
    $this->uri->segment(4), $data_pencarian);
    
    return $hasil_pagination;
  }

  public function tampilreportPenjualan($tgl_awal,$tgl_akhir)
  {
      
         $this->db->select("ph.id_jual_h, ph.no_transaksi, ph.tanggal, count(pd.kode_barang) as kode_barang, sum(pd.qty) as qty, sum(pd.jumlah) as total");
       
     $this->db->from("penjualan_header as ph");
        
          $this->db->join("penjualan_detail as pd", "on ph.id_jual_h = pd.id_jual_h");
         
          $this->db->where("ph.tanggal BETWEEN '$tgl_awal' AND '$tgl_akhir'");
      
      $this->db->group_by("ph.id_jual_h");
      
          
           $query = $this->db->get();  
        return $query->result();
    
  }
   
   public function createKodeUrut() {
	{

     
            //cek kode barang terakhir
            date_default_timezone_set("Asia/Jakarta");
              $this->db->select('MAX(no_transaksi) as no_transaksi');
              $query = $this->db->get($this->_table_header);
              $result = $query->row_array();//hasil berbentuk array
        
              $kode_transaksi_terakhir = $result['no_transaksi'];
              // format BR001 = BR (label awal), 001(nomor urut)
              
              $label = "TR";
              $labelthn = substr(date('y'),1,1);
              $labelbln = date('m');
              $labeljam = date('H');
              $rubahjam = "";
              if($labeljam % 2 ==0){
                $rubahjam = "A";
              }else{
                $rubahjam = "B";
              }
        
              $no_urut_lama = (int) substr($kode_transaksi_terakhir, 2, 3);
              $no_urut_lama ++;
        
              $no_urut_baru = sprintf("%03s", $no_urut_lama);
              $kode_transaksi_baru = $label . $labelthn . $labelbln . $labeljam . $rubahjam . $no_urut_baru;
        
              //var_dump($kode_barang_baru); die();
              //var_dump(sprintf("%03s", 0)); die();
              return $kode_transaksi_baru;
        
            }
        

        }
     }

