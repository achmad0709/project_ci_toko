<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Penjualan extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
		//load model terkait
		$this->load->model("Penjualan_model");
		$this->load->model("Barang_model");
		
			// cek login akses
		$user_login = $this->session->userdata();
		if (count($user_login) <= 1) {
			
	
		
			redirect("auth/index", "refresh");
	}
	$this->load->library('pdf');
	}

	public function index()
	{
		$this->listPenjualan();
    }
    
    public function listPenjualan()
	{
		if (isset($_POST['tombol_cari'])) {
			$data['kata_pencarian'] = $this->input->post('caridata');
			$this->session->set_userdata('session_pencarian_penjualan', $data['kata_pencarian']);
		} else {
			$data['kata_pencarian'] = $this->session->userdata('session_pencarian_penjualan');
		}
		//$data['data_penjualan'] = $this->penjualan_model->tampilDataPenjualan();
		$data['data_penjualan'] = $this->Penjualan_model->tombolpagination($data['kata_pencarian']);
        
		$data['content'] = 'forms/list_penjualan';
		$this->load->view('home2', $data);
    }
    
    public function input()
	{
      
       
        
        
        //if (!empty($_REQUEST)) {
           // $penjualan_header = $this->penjualan_model;
           // $penjualan_header->savepenjualanHeader();
            //$id_terakhir = array();
            //panggil ID transaksi terakhir
            //$id_terakhir = $penjualan_header->idTransaksiTerakhir();
           
			//redirect("penjualan/inputDetail/" . $id_terakhir, "refresh");
        //}
        $validation = $this->form_validation;
		$validation->set_rules($this->Penjualan_model->rulesinput());
		$data['no_transaksi_baru'] = $this->Penjualan_model->createKodeUrut();
        
		
		if ($validation->run()) {
			$this->Penjualan_model->savePenjualanHeader();
			$this->session->set_flashdata('info', '<div style="color : green">simpan data berhasil !</div>');
			redirect("Penjualan/index", "refresh");
		}
			
		//$this->load->view('input_supplier');
		$data['content'] = 'forms/input_penjualan_header';
		$this->load->view('home2', $data);
	}

	public function inputDetail($id_jual_h)
	{
        // panggil data barang untuk kebutuhan form input
		 $data['id_header'] = $id_jual_h;
         $data['data_barang'] = $this->Barang_model->tampilDataBarang();
         $data['data_penjualan_detail'] = $this->Penjualan_model->tampilDataPenjualanDetail($id_jual_h);
        
        //if (!empty($_REQUEST)) {
          
            //$this->penjualan_model->savePenjualanDetail($id_penjualan_header);
            
          
            //$kode_barang  = $this->input->post('kode_barang');
            //$qty        = $this->input->post('qty');
            //$this->barang_model->updateStok($kode_barang, $qty);

			//redirect("penjualan/inputDetail/" . $id_penjualan_header, "refresh");
        //}
         $validation = $this->form_validation;
		$validation->set_rules($this->Penjualan_model->rules1());
		
		if ($validation->run()) {
			$this->Penjualan_model->savePenjualanDetail($id_jual_h);
			$this->session->set_flashdata('info', '<div style="color : green">simpan data berhasil !</div>');
			$kode_barang = $this->input->post('kode_barang');
			$qty = $this->input->post('qty');
			$this->Barang_model->updatestok1($kode_barang, $qty);
			redirect("penjualan/inputDetail/" . $id_jual_h, "refresh");
		}
			
		//$this->load->view('input_supplier');
		$data['content'] = 'forms/input_penjualan_detail';
		$this->load->view('home2', $data);
        
		
		//$this->load->view('input_penjualan_detail', $data);
	}
		
	
    public function report1() {
		$data['content'] = 'forms/report1';
		$this->load->view('home2', $data);

	}
	public function report_penjualan()
	{
		if (!empty($_REQUEST)) {
		//$tgl_awal=$this->input->post('tgl_awal');
		//$pisah=explode('/', $tgl_awal);
		//$array=array($pisah[2],$pisah[0],$pisah[1]);
		//$tgl_awal=implode('-', $array);

		//$tgl_akhir=$this->input->post('tgl_akhir');
		//$pisah=explode('/', $tgl_akhir);
		//$array=array($pisah[2],$pisah[0],$pisah[1]);
		//$tgl_akhir=implode('-', $array);
        
		$tgl_awal  = $this->input->post('tgl_awal');
	    $tgl_akhir = $this->input->post('tgl_akhir');
		$data['data_penjualan'] = $this->Penjualan_model->tampilreportpenjualan($tgl_awal,$tgl_akhir);
		$data['tgl_awal'] = $tgl_awal;
		$data['tgl_akhir'] = $tgl_akhir;
		
		$data['content'] = 'forms/report_penjualan';
		$this->load->view('home2', $data);
		} else {
			redirect("Penjualan/laporan/", "refresh");
		}

	}
	
public function cetak($tgl_awal, $tgl_akhir)
   {
        $pdf = new FPDF('P','mm','A4');
        // membuat halaman baru
        $pdf->AddPage();
        // setting jenis font yang akan digunakan
        $pdf->SetFont('Arial', 'B' ,15);
        // mencetak string 
        $pdf->Cell(187, 7, 'LAPORAN DATA PENJUALAN', 0, 1, 'C');
        $pdf->SetFont('Arial','B',15);
        $pdf->Cell(190,7,'TOKO JAYA ABADI DARI '.$tgl_awal.' S/D '.$tgl_akhir ,0,1,'C');
        // Memberikan space kebawah agar tidak terlalu rapat
       $pdf->Cell(10,10,'',0,1,'L');
        $pdf->SetFont('Arial','B',10);
        $pdf->Cell(18, 6, 'No', 1, 0, 'C');
         $pdf->Cell(22, 6, 'Id penjualan ', 1, 0, 'C');
        $pdf->Cell(35, 6, 'Nomor transaksi', 1, 0, 'C');
        $pdf->Cell(30,6,'Tanggal',1,0,'C');
       
        $pdf->Cell(30,6,'Total barang',1,0,'C');
        $pdf->Cell(30,6,'Total qty',1,0,'C');
        $pdf->Cell(29,6,'jumlah nominal',1,1,'C');
         
        $pdf->SetFont('Arial','B',10);
        $no = 0;
        $total = 0;
        $report_penjualan = $this->Penjualan_model->tampilreportpenjualan($tgl_awal,$tgl_akhir);
       
        foreach ($report_penjualan as $data){
            $no ++;
             $pdf->Cell(18,6,$no,1,0,'C');
              $pdf->Cell(22,6,$data->id_jual_h,1,0,'C');
            $pdf->Cell(35,6,$data->no_transaksi,1,0,'C');
            $pdf->Cell(30,6,$data->tanggal,1,0,'C');
            $pdf->Cell(30,6,$data->kode_barang,1,0,'C'); 
            $pdf->Cell(30,6,$data->qty,1,0,'C'); 
            $pdf->Cell(29,6,'Rp.'. number_format($data->total),1,1,'R');

              $total += $data->total; 
        }
        $pdf->SetFont('Arial','B',10);
        $pdf->Cell(165,6,'total PENJUALAN keseluruhan',1,0,'C');
        $pdf->Cell(29,6,'Rp.'. number_format($total),1,1,'R');
        $pdf->Output();
    }

   
}






