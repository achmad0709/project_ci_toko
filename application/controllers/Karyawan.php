<?php defined('BASEPATH') OR exit('No direct script acces allowed');

class Karyawan extends CI_controller {

	public function __construct()
	
	{
		
		parent::__construct();
		//load model terkait
		$this->load->model("Karyawan_model");
		$this->load->model("jabatan_model");
		
		//cek sesi login
		$user_login =$this->session->userdata();
		if (count($user_login)<=1){
			redirect("auth/index", "refresh");
		}
		
	}
	
	public function index()
	
	{
		$this->listKaryawan();
	}
	
	public function listKaryawan()
	
	{
	// proses cari data
	if (isset($_POST['tombol_cari'])) {
		$data['kata_pencarian'] = $this->input->post('caridata');
		$this->session->set_userdata('session_pencarian_karyawan', $data['kata_pencarian']);
	} else {
		$data['kata_pencarian'] = $this->session->userdata('session_pencarian_karyawan');
	}
	
	
	{
		$data['data_karyawan'] = $this->Karyawan_model->tombolpagination($data['kata_pencarian']);
		//$data['data_karyawan'] = $this->Karyawan_model->tampilDataKaryawan();
		$data['content'] = 'forms/list_karyawan';
		$this->load->view('home2', $data);
	}
	}
	
		public function input()
	{
		$data['data_jabatan'] = $this->jabatan_model->tampilDataJabatan();
		$data['kode_nik_baru'] = $this->Karyawan_model->createNikUrut();
		
		$data['content'] = 'forms/InputKaryawan';
		
		/*if (!empty($_REQUEST)) {
			$m_karyawan = $this->Karyawan_model;
			$m_karyawan->save();
			redirect("Karyawan/index", "refresh"); 
		}*/
		$validation = $this->form_validation;
		$validation->set_rules($this->Karyawan_model->rules());
		$validation->set_rules($this->Karyawan_model->rulesinput());
		
		
		if ($validation->run()) {
			$this->Karyawan_model->save();
			$this->session->set_flashdata('info', '<div style="color: green">Simpan Data Berhasil !</div>');
			redirect("Karyawan/index", "refresh");
		}
		
		$this->load->view('home2', $data);
	}
	
	   public function detailkaryawan($nik)
	   {
			$data['detail_karyawan']	= $this->Karyawan_model->detail($nik);
			$data['content']		= 'forms/detail_karyawan';
			$this->load->view('home2', $data);   
	   }
	   
	   public function Editkaryawan($nik)
	{
		$data['data_jabatan'] = $this->jabatan_model->tampilDataJabatan();
		$data['detail_karyawan'] = $this->Karyawan_model->detail($nik);
		$data['content'] = 'forms/Editkaryawan';
		
		/*if (!empty($_REQUEST)) {
			$m_karyawan = $this->Karyawan_model;
			$m_karyawan->update($nik);
			redirect("Karyawan/index", "refresh"); 
		}*/
		$validation = $this->form_validation;
		$validation->set_rules($this->Karyawan_model->rules());
		
		if ($validation->run()) {
			$this->Karyawan_model->update($nik);
			$this->session->set_flashdata('info', '<div style="color: green">Simpan Data Berhasil !</div>');
			redirect("Karyawan/index", "refresh");
		}
		
		
		$this->load->view('home2', $data);
	}
	
	public function deletekaryawan($nik)
	{
		$m_karyawan = $this->Karyawan_model;
		$m_karyawan->delete($nik);
		redirect("Karyawan/index", "refresh");
		
	}
	
	   
}

