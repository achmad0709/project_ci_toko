<head>
  <form id="forms" method="POST" action="<?=base_url()?>pembelian/report_pembelian">
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <title>jQuery UI Datepicker - Default functionality</title>
  <link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
  <link rel="stylesheet" href="/resources/demos/style.css">
  <script src="https://code.jquery.com/jquery-1.12.4.js"></script>
  <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
  <script>
  $( function() {
   $( "#tgl_awal" ).datepicker({dateFormat: "yy-mm-dd"});
    $( "#tgl_akhir" ).datepicker({dateFormat: "yy-mm-dd"});
  } );
  </script>
</head>
<center>

<body>
	<table width="100%" height="40" border="0" align="center">
	<tr>
		<td colspan="5" align="center"><b>"Laporan Pembelian"</b></td>
	</tr>
	</table>
	</br>
	
	<p align="center">tanggal mulai : <input type="text" id="tgl_awal" name="tgl_awal"></p></br>
	<p align="center">tanggal akhir : <input type="text" id="tgl_akhir" name="tgl_akhir"></p></br>
	
	<p align="center">
	<input type="submit" value="proses" name="proses" id="proses"></a></p>
</body>

</form>
 

<script>
	$(document).ready(function() {
		$('#proses').on('click', function(event){
			event.preventDefault();
			var tgl_awal = $('#tgl_awal').val();
			var tgl_akhir = $('#tgl_akhir').val();

			if (tgl_awal == '' || tgl_akhir == '') {
				alert('Tanggal Tidak Boleh Kosong');
			}else if (new Date (tgl_awal) > new Date(tgl_akhir)){
				alert('Format Waktu Salah Input');
			}else{
				$('#forms').submit();
			}
		});
		});
</script> 