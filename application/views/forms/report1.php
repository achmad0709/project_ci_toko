<head>
 <form id="forms" method="POST" action="<?=base_url();?>Penjualan/report_penjualan">
 <link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
  <link rel="stylesheet" href="/resources/demos/style.css">
  <script src="https://code.jquery.com/jquery-1.12.4.js"></script>
  <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
<script>
  $( function() {
    $( "#tgl_awal" ).datepicker({dateFormat: "yy-mm-dd"});
    $( "#tgl_akhir" ).datepicker({dateFormat: "yy-mm-dd"});
  } );
  </script>
</head>
<body>
  <table width="100%" height="40" border="0" align="center">
<tr>
<td colspan="5" align="center"><b>"Laporan Penjualan"</b></

  </tr>
  </table>
  <br></br>

<p align="center">tanggal mulai:<input type="text" name="tgl_awal" id="tgl_awal"></p><br>

<p align="center">tanggal akhir:<input type="text" name="tgl_akhir" id="tgl_akhir"></p>

<p align="center"><input type="submit" value="proses" name="proses" id="proses"></a></p>
</body>
</form>
<script>
$(document).ready(function(){
  $('#proses').on('click', function(event) {
    event.preventDefault();
    var tgl_awal = $('#tgl_awal').val();
    var tgl_akhir = $('#tgl_akhir').val();
    if (tgl_awal == '' || tgl_akhir == '') {
      alert('tanggal tidak boleh kosong');
    }else if (new Date(tgl_awal) > new Date(tgl_akhir)) {
      alert('format waktu salah input');
    } else {
      $('#forms').submit();
    }
  });
  
});
</script>